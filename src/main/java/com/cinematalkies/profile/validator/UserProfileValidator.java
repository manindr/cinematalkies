package com.cinematalkies.profile.validator;

/*
 * Author:Maninder Singh
 */

import com.cinematalkies.common.utils.EmailUtils;
import com.cinematalkies.profile.model.enums.ApiMessageCodeEnum;
import com.cinematalkies.profile.model.exception.UserProfileException;
import com.cinematalkies.profile.model.request.SignUpRequest;
import com.cinematalkies.profile.service.UserProfileService;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Service
public class UserProfileValidator {
    private static final Logger logger = LogManager.getLogger(UserProfileValidator.class);

    private static final List<String> allowedCellStartingDigit = Arrays.asList("7", "8", "9");
    private static final short CONTACT_NO_LENGTH = 10;
    private static final int MAX_EMAIL_LENGTH = 32;

    @Autowired
    private UserProfileService userProfileService;

    public void validateSignUpRequest(SignUpRequest signUpRequest) {
        logger.info("validating signup request: " + signUpRequest);
        validateEmailFormat(signUpRequest.getEmail());
        validateEmail(signUpRequest.getEmail());
        validatePassword(signUpRequest);
        validateContactNoFormat(signUpRequest.getCell());
    }

    private void validatePassword(SignUpRequest signUpRequest) {
        if (StringUtils.isBlank(signUpRequest.getPasscode())) {
            logger.error("password is blank for " + signUpRequest.getEmail());
            throw new UserProfileException(ApiMessageCodeEnum.BLANK_PASSWORD);
        }
    }

    public void validateEmailFormat(String email) {
        logger.info("validating: " + email);
        if (StringUtils.isBlank(email)) {
            logger.error("email is null");
            throw new UserProfileException(ApiMessageCodeEnum.BLANK_EMAIL);
        }
        logger.info("validating format");
        if (!EmailUtils.validateEmailFormat(email)) {
            logger.error("invalid email format");
            throw new UserProfileException(ApiMessageCodeEnum.INTERNAL_SERVER_ERROR);
        }
        if (email.length() > MAX_EMAIL_LENGTH) {
            throw new UserProfileException(ApiMessageCodeEnum.EMAIL_TOO_LONG);
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void validateEmail(String email) {

        logger.info("validating email exists or not");

        if (userProfileService.getMember(email) != null) {
            logger.error("user already exists " + email);
            throw new UserProfileException(ApiMessageCodeEnum.EMAIL_ALREADY_REGISTERED);
        }
    }


    public void validateContactNoFormat(String contactNo) {

        logger.info("contact no format validation");
        if (StringUtils.isBlank(contactNo))
            throw new UserProfileException(ApiMessageCodeEnum.BLANK_CELL);

        // format validation
        logger.info("Contact no:" + contactNo);
        try {
            Long.parseLong(contactNo);
        } catch (Exception e) {
            logger.error("invalid cell number format");
//            throw new InvalidCellNumberFormatException();
        }

        if (contactNo.length() != CONTACT_NO_LENGTH) {
            logger.error("invalid cell number length");
            //          throw new InvalidCellNumberLengthException();
        }

        String firstDigit = String.valueOf(contactNo.charAt(0));
        if (!allowedCellStartingDigit.contains(firstDigit)) {
            logger.error("invalid cell number");
            //        throw new InvalidCellNumberException();
        }
    }


}
