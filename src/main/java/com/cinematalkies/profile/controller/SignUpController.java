package com.cinematalkies.profile.controller;

/*
 * Author: Maninder Singh
 */

import com.cinematalkies.profile.model.request.SignUpRequest;
import com.cinematalkies.profile.model.response.RestApiResponse;
import com.cinematalkies.profile.service.SignUpService;
import com.cinematalkies.profile.service.UserProfileService;
import com.cinematalkies.profile.validator.UserProfileValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/account/sign-up")
public class SignUpController extends ControllerBaseUserProfile {

    private static final Logger logger = LogManager.getLogger(SignUpController.class);

    @Autowired
    UserProfileValidator validator;

    @Autowired
    SignUpService signUpService;

    @Autowired
    UserProfileService userProfileService;


    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    RestApiResponse signUp(@RequestBody SignUpRequest signUpRequest) {

        validator.validateSignUpRequest(signUpRequest);
        signUpService.signUp(signUpRequest);
        return RestApiResponse.buildSuccess();
    }
}
