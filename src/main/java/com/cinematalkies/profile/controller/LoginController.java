package com.cinematalkies.profile.controller;

import com.cinematalkies.profile.model.request.LoginRequest;
import com.cinematalkies.profile.model.response.RestApiResponse;
import com.cinematalkies.profile.service.LoginService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


/*
 * Author: Maninder Singh
 */

@Controller
@RequestMapping("/account/login")
public class LoginController extends ControllerBaseUserProfile{

    private static final Logger logger = LogManager.getLogger(LoginController.class);

    @Autowired
    LoginService loginService;

    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    RestApiResponse login(@RequestBody LoginRequest loginRequest) {
        logger.info("Login request received: " + loginRequest);
        loginService.processLogin(loginRequest);
        return RestApiResponse.buildSuccess();
    }

}
