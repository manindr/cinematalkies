package com.cinematalkies.profile.controller;


import com.cinematalkies.profile.model.enums.ApiMessageCodeEnum;
import com.cinematalkies.profile.model.exception.UserProfileException;
import com.cinematalkies.profile.model.response.MessageApiResponse;
import com.cinematalkies.profile.model.response.RestApiResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletResponse;

/**
 * The base Exception handling class to be derived by all controllers.
 *
 * @author tiru
 */
@Component
public class
ControllerBaseUserProfile {

    private static Logger logger = LogManager.getLogger(ControllerBaseUserProfile.class);

    /**
     * Method to catch all business exceptions defined explicitly in the exception package and extend
     * the base UserProfileException class.
     *
     * @param exception
     * @param response
     * @return
     */
    @ExceptionHandler(value = {UserProfileException.class})
    protected
    @ResponseBody
    RestApiResponse handleAccountException(UserProfileException exception, HttpServletResponse response) {

        ApiMessageCodeEnum errorCode = exception.getErrorCode();
        response.setStatus(HttpStatus.OK.value());
        return RestApiResponse.buildFail(buildMessageApiResponse(errorCode));
    }

    /**
     * Generic Exception handling class, when something really goes wrong.
     *
     * @param exception
     * @return
     */
    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected
    @ResponseBody
    RestApiResponse genericExceptionHandler(Exception exception) {
        logger.error(exception, exception);
        return RestApiResponse.buildFail(buildMessageApiResponse(ApiMessageCodeEnum.INTERNAL_SERVER_ERROR));
    }

    private MessageApiResponse buildMessageApiResponse(ApiMessageCodeEnum errorCode) {
        String code = errorCode.getErrorCode();
        String text = errorCode.getErrorMessage();
        return MessageApiResponse.build(code, text);
    }
}
