package com.cinematalkies.profile.controller;

import com.cinematalkies.cinema.model.entity.ShowTime;
import com.cinematalkies.cinema.model.entity.Theatre;
import com.cinematalkies.profile.model.request.AddMovieToTheatreRequest;
import com.cinematalkies.profile.model.request.MovieRequest;
import com.cinematalkies.profile.model.request.TheatreRequest;
import com.cinematalkies.profile.model.response.RestApiResponse;
import com.cinematalkies.profile.service.AdminService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/*
 * Author: Maninder Singh
 */

@Controller
@RequestMapping("/admin")
public class AdminController extends ControllerBaseUserProfile {

    private static final Logger logger = LogManager.getLogger(AdminController.class);

    @Autowired
    AdminService adminService;


    @RequestMapping(value = "/movie", method = RequestMethod.POST)
    public
    @ResponseBody
    RestApiResponse addMovie(@RequestBody MovieRequest movieRequest) {
        adminService.addMovie(movieRequest);
        return RestApiResponse.buildSuccess();
    }

    @RequestMapping(value = "/theatre", method = RequestMethod.POST)
    public
    @ResponseBody
    RestApiResponse addTheatre(@RequestBody TheatreRequest theatreRequest) {
        adminService.addTheatre(theatreRequest);
        return RestApiResponse.buildSuccess();
    }


    @RequestMapping(value = "/theatre", method = RequestMethod.GET)
    public
    @ResponseBody
    RestApiResponse getTheatres() {
        List<Theatre> theatreList = adminService.getTheatres();
        return RestApiResponse.buildSuccess(theatreList);
    }


    @RequestMapping(value = "/theatre/shows/{id}", method = RequestMethod.GET)
    public
    @ResponseBody
    RestApiResponse getTheatreShowTimes(@PathVariable("id") int theatreId) {
        List<ShowTime> showTimeList= adminService.getShowsForTheatre(theatreId);
        return RestApiResponse.buildSuccess(showTimeList);
    }

    @RequestMapping(value = "/movieToTheatre", method = RequestMethod.POST)
    public
    @ResponseBody
    RestApiResponse addMovieToTheatre(@RequestBody AddMovieToTheatreRequest movieToTheatreRequest) {
        adminService.addMovieToTheatre(movieToTheatreRequest);
        return RestApiResponse.buildSuccess();
    }

    @RequestMapping(value = "/cancelUnpaidBookings/{showId}", method = RequestMethod.DELETE)
    public
    @ResponseBody
    RestApiResponse cancelUnpaidBookings(@PathVariable("showId") int showId) {
        adminService.cancelUnpaidBookingsForShow(showId);
        return RestApiResponse.buildSuccess();
    }


    @RequestMapping(value = "/pay/{bookingId}", method = RequestMethod.POST)
    public
    @ResponseBody
    RestApiResponse payForBooking(@PathVariable("bookingId") int bookingId) {
        adminService.payForBooking(bookingId);
        return RestApiResponse.buildSuccess();
    }



}
