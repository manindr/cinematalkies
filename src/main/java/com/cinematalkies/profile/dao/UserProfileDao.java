package com.cinematalkies.profile.dao;

import com.cinematalkies.cinema.model.entity.Screen;
import com.cinematalkies.cinema.model.entity.Seat;
import com.cinematalkies.profile.model.entity.Member;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by maninder on 22/1/17.
 */
@Repository
public class UserProfileDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }


    public Member getMember(String email) {
        return (Member) getCurrentSession().get(Member.class, email);
    }


    public void save(Object member) {
        getCurrentSession().save(member);
    }

    public void save(Seat member) {
        getCurrentSession().save(member);
    }

    public Screen getScreen(Integer screenId) {
        return (Screen) getCurrentSession().get(Screen.class, screenId);
    }}
