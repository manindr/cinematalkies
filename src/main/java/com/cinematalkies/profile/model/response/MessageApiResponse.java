package com.cinematalkies.profile.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(Include.NON_NULL)
public class MessageApiResponse {

	private String code;
	private String text;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getText() {
		return text;
	}

	public MessageApiResponse(String code, String text) {
		this.code = code;
		this.text = text;
	}

	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Static builder for building the MessageApiResponse.
	 * 
	 * @param code
	 * @param text
	 * @return
	 */
	public static MessageApiResponse build(String code, String text) {
		MessageApiResponse messageApiResponse = new MessageApiResponse(code, text);
		return messageApiResponse;
	}

	public Map<String, String> toMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("code", this.code);
		map.put("text", this.text);
		return map;
	}

}
