package com.cinematalkies.profile.model.enums;

public enum ApiMessageCodeEnum {

    UNAUTHORIZED_ACCESS("401", "Unathorized acess"),
    INTERNAL_SERVER_ERROR("500", "Something went wrong. please try again later"),
    INVALID_EMAIL_FORMAT("001", "Invalid Email Format"),
    INVALID_CELL_NUMBER("002", "Invalid CellNumber"),
    INVALID_CELL_FORMAT("005", "Invalid CellNumber Format"),
    INVALID_CELL_LENGTH("006", "Invalid CellNumber Length"),
    INVALID_EMAIL("125", "Invalid Email Id"),
    EMAIL_TOO_LONG("003", "Email is too long"),
    INVALID_LOGIN_ID("007", "Invalid login ID"),
    INVALID_PASSWORD("008", "Invalid password"),
    EMAIL_ALREADY_REGISTERED("102", "Email is already registered"),
    BLANK_PASSWORD("103", "Password can not be blank."),
    BLANK_CELL("104", "Cell number can not be blank"),
    BLANK_EMAIL("105", "Email can't be blank"),
    SHOW_NOT_AVAILABLE("106", "Show is Full"),
    SEAT_NOT_AVAILABLE("107", "One or more selected seats are not available"),
    INVALID_CARD("108", "Card details are invalid"),
    BOOKING_NOT_EXIST("109", "Booking id is not exists"),
    BOOKING_NOT_YET("110", "Why you Not Book a signle movie???");
    private String errorMessage;
    private String errorCode;

    ApiMessageCodeEnum(String code, String message) {
        this.errorCode = code;
        this.errorMessage = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
