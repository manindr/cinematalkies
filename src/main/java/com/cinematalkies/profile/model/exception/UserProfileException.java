package com.cinematalkies.profile.model.exception;

import com.cinematalkies.profile.model.enums.ApiMessageCodeEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class UserProfileException extends RuntimeException {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  private static final Logger logger = LogManager.getLogger(UserProfileException.class);

  private ApiMessageCodeEnum errorCode;

  public UserProfileException(ApiMessageCodeEnum errorCode) {

    super();
    this.errorCode = errorCode;
    logger
        .info("Inside UserProfileException constructor for Exception Handling" + errorCode.name());

  }

  public ApiMessageCodeEnum getErrorCode() {
    return errorCode;
  }

}
