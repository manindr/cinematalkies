package com.cinematalkies.profile.model.request;

import com.cinematalkies.cinema.model.entity.Address;

/**
 * Created by maninder on 24/1/17.
 */
public class TheatreRequest {

    private String name;

    private Address address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
