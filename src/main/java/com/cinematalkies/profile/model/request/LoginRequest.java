package com.cinematalkies.profile.model.request;


import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginRequest {

    private String email;

    @JsonProperty("pwd")
    private String passcode;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasscode() {
        return passcode;
    }

    public void setPasscode(String passcode) {
        this.passcode = passcode;
    }

    public static LoginRequest buildLoginRequest(String email, String pwd) {
        LoginRequest request = new LoginRequest();
        request.setEmail(email);
        request.setPasscode(pwd);
        return request;

    }
}
