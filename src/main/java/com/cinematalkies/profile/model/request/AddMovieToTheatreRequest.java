package com.cinematalkies.profile.model.request;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by maninder on 25/1/17.
 */
public class AddMovieToTheatreRequest {

    private int theatreId;
    private int movieId;
    private List<Timestamp> showTimes;

    public int getTheatreId() {
        return theatreId;
    }

    public void setTheatreId(int theatreId) {
        this.theatreId = theatreId;
    }



    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public List<Timestamp> getShowTimes() {
        return showTimes;
    }

    public void setShowTimes(List<Timestamp> showTimes) {
        this.showTimes = showTimes;
    }
}
