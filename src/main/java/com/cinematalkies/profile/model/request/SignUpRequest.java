package com.cinematalkies.profile.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by maninder on 22/1/17.
 */
public class SignUpRequest {

    private String email;

    @JsonProperty("pwd")
    private String passcode;

    private String address;

    private String cell;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasscode() {
        return passcode;
    }

    public void setPasscode(String passcode) {
        this.passcode = passcode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCell() {
        return cell;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }
}
