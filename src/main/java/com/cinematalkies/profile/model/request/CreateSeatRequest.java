package com.cinematalkies.profile.model.request;


import com.cinematalkies.cinema.model.entity.Seat;

public class CreateSeatRequest {

  private Integer id;
  boolean isAvailable;
  boolean isVoid;
  double price;
  private Integer xPosition;
  private Integer yPosition;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public boolean isVoid() {
    return isVoid;
  }

  public void setVoid(boolean aVoid) {
    isVoid = aVoid;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public Integer getxPosition() {
    return xPosition;
  }

  public void setxPosition(Integer xPosition) {
    this.xPosition = xPosition;
  }

  public Integer getyPosition() {
    return yPosition;
  }

  public void setyPosition(Integer yPosition) {
    this.yPosition = yPosition;
  }

  public static Seat make(CreateSeatRequest createSeatRequest) {
    Seat seat = new Seat();
    seat.setVoid(createSeatRequest.isVoid());
    seat.setPrice(createSeatRequest.getPrice());
    seat.setId(createSeatRequest.getId());
    return seat;
  }
}
