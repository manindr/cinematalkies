package com.cinematalkies.profile.service;

import com.cinematalkies.cinema.dao.MovieDao;
import com.cinematalkies.cinema.model.BookingStatusEnum;
import com.cinematalkies.cinema.model.PayModeType;
import com.cinematalkies.cinema.model.entity.*;
import com.cinematalkies.profile.model.request.AddMovieToTheatreRequest;
import com.cinematalkies.profile.model.request.MovieRequest;
import com.cinematalkies.profile.model.request.TheatreRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by aman on 28/9/16.
 */

@Service
public class AdminService {

    private static Logger logger = LogManager.getLogger(AdminService.class);
    @Autowired
    private MovieDao movieDao;

    @Transactional
    public List<Theatre> getTheatres() {
        return movieDao.getAllDatas(Theatre.class);
    }


    @Transactional
    public void addMovie(MovieRequest movieRequest) {
        Movie movie = Movie.build(movieRequest);
        movieDao.save(movie);
    }

    @Transactional
    public void addTheatre(TheatreRequest theatreRequest) {
        Address address = theatreRequest.getAddress();
        movieDao.save(address);
        Screen screen1 = addScreen("Screen 1");
        Screen screen2 = addScreen("Screen 2");
        Theatre theatre = Theatre.build(theatreRequest);
        theatre.setScreens(Arrays.asList(screen1, screen2));
        movieDao.save(theatre);
        screen1.setTheatre(theatre);
        screen2.setTheatre(theatre);
    }

    private Screen addScreen(String name) {
        Screen screen = Screen.build(name);
        movieDao.save(screen);
        return screen;
    }

    @Transactional
    public void addMovieToTheatre(AddMovieToTheatreRequest movieToTheatreRequest) {
        Theatre theatre = (Theatre) movieDao.getEntityFromId(Theatre.class, movieToTheatreRequest.getTheatreId());
        Movie movie = (Movie) movieDao.getEntityFromId(Movie.class, movieToTheatreRequest.getMovieId());
        List<ShowTime> showTimeList = createShowTimes(theatre.getScreens().get(0), movieToTheatreRequest.getShowTimes(), movie);
        List<ShowTime> showTimes = theatre.getShowTimes();
        if (CollectionUtils.isEmpty(showTimes)) {
            theatre.setShowTimes(showTimes);
        }
        showTimes.addAll(showTimeList);
        theatre.getMovies().add(movie);
        movie.getTheatres().add(theatre);
    }

    private List<ShowTime> createShowTimes(Screen screen, List<Timestamp> timestamps, Movie movie) {
        List<ShowTime> showTimeList = new ArrayList<>();
        for (Timestamp timestamp : timestamps) {
            ShowTime showTime = ShowTime.build(screen, timestamp, movie);
            movieDao.save(showTime);
            List<Seat> seats = buildSeats();
            showTime.setSeats(seats);
            showTimeList.add(showTime);
        }
        return showTimeList;
    }


    private List<Seat> buildSeats() {
        List<Seat> seats = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 5; j++) {
                Seat seat = Seat.build(SeatType.SILVER, 200.0, i, j);
                movieDao.save(seat);
                seats.add(seat);
            }
        }

        for (int i = 2; i < 4; i++) {
            for (int j = 0; j < 5; j++) {
                Seat seat = Seat.build(SeatType.GOLD, 400.0, i, j);
                movieDao.save(seat);
                seats.add(seat);
            }
        }
        for (int i = 4; i < 6; i++) {
            for (int j = 0; j < 5; j++) {
                Seat seat = Seat.build(SeatType.PLATINUM, 600.0, i, j);
                movieDao.save(seat);
                seats.add(seat);
            }
        }

        return seats;
    }

    @Transactional
    public void cancelUnpaidBookingsForShow(int showId) {
        ShowTime showTime = (ShowTime) movieDao.getEntityFromId(ShowTime.class, showId);
        if (showTime == null)
            return;
        List<Booking> bookings = showTime.getBookings();
        if (CollectionUtils.isEmpty(bookings)) {
            return;
        }
        for (Booking booking : bookings) {
            if (booking.getPayMode() == PayModeType.COT.getId() && booking.getStatus() != BookingStatusEnum.BOOKED.getId()) {
                booking.setStatus(BookingStatusEnum.CANCELLED.getId());
                List<Seat> seats = booking.getSeats();
                booking.setSeats(null);
                cancelSeats(seats);
            }
        }
    }

    private void cancelSeats(List<Seat> seats) {
        if (CollectionUtils.isEmpty(seats))
            return;
        for (Seat seat : seats) {
            seat.setAvailable(true);
        }
    }

    @Transactional
    public void payForBooking(int bookingId) {
        Booking booking = (Booking) movieDao.getEntityFromId(Booking.class, bookingId);
        if (booking == null || booking.getStatus() == BookingStatusEnum.BOOKED.getId() || booking.getStatus() == BookingStatusEnum.CANCELLED.getId()) {
            return;
        }
        booking.setStatus(BookingStatusEnum.BOOKED.getId());
    }

    @Transactional
    public List<ShowTime> getShowsForTheatre(int theatreId) {
        Theatre theatre = (Theatre) movieDao.getEntityFromId(Theatre.class, theatreId);
        if (theatre == null)
            return null;
        List<ShowTime> showTimes = theatre.getShowTimes();
        if (CollectionUtils.isEmpty(showTimes))
            return null;
        List<ShowTime> showTimeList = new ArrayList<>();
        for (ShowTime showTime : showTimes) {
            showTimeList.add(buildShow(showTime));
        }
        return showTimeList;
    }

    private ShowTime buildShow(ShowTime showTime) {
        ShowTime show = new ShowTime();
        show.setId(showTime.getId());
        show.setDate(showTime.getDate());
        return show;
    }
}
