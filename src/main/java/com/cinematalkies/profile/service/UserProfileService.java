package com.cinematalkies.profile.service;

import com.cinematalkies.cinema.model.entity.Screen;
import com.cinematalkies.cinema.model.entity.Seat;
import com.cinematalkies.profile.dao.UserProfileDao;
import com.cinematalkies.profile.model.entity.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by maninder on 22/1/17.
 */
@Service
public class UserProfileService {

    @Autowired
    private UserProfileDao userProfileDao;

    @Transactional
    public void save(Member member) {
      userProfileDao.save(member);
    }

    @Transactional
    public void save(Seat seat) {
        userProfileDao.save(seat);
    }


    @Transactional
    public void save(Screen seat) {
        userProfileDao.save(seat);
    }

    @Transactional
    public Member getMember(String email) {
        return userProfileDao.getMember(email);
    }


    @Transactional
    public Screen getScreen(int screenId) {
        return userProfileDao.getScreen(screenId);
    }
}
