package com.cinematalkies.profile.service;

import com.cinematalkies.common.utils.HashingUtil;
import com.cinematalkies.profile.model.entity.Member;
import com.cinematalkies.profile.model.enums.ApiMessageCodeEnum;
import com.cinematalkies.profile.model.exception.UserProfileException;
import com.cinematalkies.profile.model.request.LoginRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by aman on 28/9/16.
 */

@Service
public class LoginService {

    private static Logger logger = LogManager.getLogger(LoginService.class);
    @Autowired
    private UserProfileService userProfileService;


    public void processLogin(LoginRequest loginRequest) {
        Member member = userProfileService.getMember(loginRequest.getEmail());
        if (member == null) {
            logger.error("no member found for " + loginRequest.getEmail());
            throw new UserProfileException(ApiMessageCodeEnum.INVALID_LOGIN_ID);
        }
        String passcode = loginRequest.getPasscode();
        if (StringUtils.isBlank(passcode)) {
            logger.error("password is blank for " + loginRequest.getEmail());
            throw new UserProfileException(ApiMessageCodeEnum.BLANK_PASSWORD);
        }

        boolean b = HashingUtil.doesHashMatch(passcode, member.getPassword());
        if (!b) {
            logger.error("password mismatch for " + loginRequest.getEmail());
            throw new UserProfileException(ApiMessageCodeEnum.INVALID_PASSWORD);
        }
    }

}
