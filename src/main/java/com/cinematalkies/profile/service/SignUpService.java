package com.cinematalkies.profile.service;

import com.cinematalkies.common.utils.HashingUtil;
import com.cinematalkies.profile.model.entity.Member;
import com.cinematalkies.profile.model.request.SignUpRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SignUpService {

    private final Logger logger = LogManager.getLogger(this.getClass());

    @Autowired
    UserProfileService userProfileService;

    public Member signUp(SignUpRequest signUpRequest) {
        logger.info("creating member");
        Member member = new Member();
        member.setEmail(signUpRequest.getEmail());
        String password = signUpRequest.getPasscode();
        member.setPassword(HashingUtil.encodeMD5AndBecrypt(password));
        member.setCell(signUpRequest.getCell());
        member.setAddress(signUpRequest.getAddress());
        member.setCreatedAt(new Date());
        userProfileService.save(member);
        return member;
    }
}
