package com.cinematalkies.cinema.utils;

import com.cinematalkies.cinema.model.entity.Seat;

import java.util.List;

/**
 * Created by maninder on 23/1/17.
 */
public class PaymentUtils {

    public static Double getTotalPrice(List<Seat> seats) {
        Double totalAmnt = 0d;
        for (Seat seat : seats) {
            totalAmnt = totalAmnt + seat.getPrice();
        }
        return totalAmnt;
    }

}
