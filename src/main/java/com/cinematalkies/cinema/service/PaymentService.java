package com.cinematalkies.cinema.service;

import com.cinematalkies.cinema.model.PayModeType;
import com.cinematalkies.cinema.model.request.PayModeRequest;
import com.cinematalkies.cinema.utils.CardUtils;
import com.cinematalkies.profile.model.enums.ApiMessageCodeEnum;
import com.cinematalkies.profile.model.exception.UserProfileException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by maninder on 23/1/17.
 */
@Service
public class PaymentService {
    public void doPayment(PayModeRequest payModeRequest, Double amount) {
        if (PayModeType.ONLINE_PAYMENT.getId() == payModeRequest.getPayModeId()) {
            validateBankingDetails(payModeRequest.getBankingDetails());
        }
    }

    private String validateBankingDetails(Map<String, Object> bankingDetails) {
        String card = (String) bankingDetails.get("card");
        if (StringUtils.isBlank(card)) {
            throw new UserProfileException(ApiMessageCodeEnum.INVALID_CARD);
        }
        if (!CardUtils.isValidCard(card)) {
            throw new UserProfileException(ApiMessageCodeEnum.INVALID_CARD);
        }
        return card;
    }

}
