package com.cinematalkies.cinema.service;

import com.cinematalkies.cinema.dao.MovieDao;
import com.cinematalkies.cinema.model.BookingStatusEnum;
import com.cinematalkies.cinema.model.PayModeType;
import com.cinematalkies.cinema.model.entity.*;
import com.cinematalkies.cinema.model.request.BookingRequest;
import com.cinematalkies.cinema.model.response.BookingDetail;
import com.cinematalkies.cinema.model.response.MovieShowTimeResponse;
import com.cinematalkies.cinema.model.response.SeatDataResponse;
import com.cinematalkies.cinema.model.response.SeatLayoutResponse;
import com.cinematalkies.profile.model.entity.Member;
import com.cinematalkies.profile.model.enums.ApiMessageCodeEnum;
import com.cinematalkies.profile.model.exception.UserProfileException;
import com.cinematalkies.profile.service.UserProfileService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.cinematalkies.cinema.utils.PaymentUtils.getTotalPrice;

/**
 * Created by maninder on 22/1/17.
 */
@Service
public class MovieService {

    private double FEE_PERCENT = .05d;

    @Autowired
    private MovieDao movieDao;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private SeatService seatService;

    @Autowired
    UserProfileService userProfileService;

    @Transactional
    public void updateEntity(Object o) {
        movieDao.updateEntity(o);
    }

    @Transactional
    public List getAllDatas(Class T) {
        return movieDao.getAllDatas(T);
    }

    @Transactional
    public List<MovieShowTimeResponse> getShowsForMovieId(int movieId) {
        List<MovieShowTimeResponse> movieShowTimeResponseList = new ArrayList<>();
        Movie movie = movieDao.getMovie(movieId);
        if (movie == null) {
            return movieShowTimeResponseList;
        }
        List<Theatre> theatreList = movie.getTheatres();
        if (CollectionUtils.isEmpty(theatreList)) {
            return movieShowTimeResponseList;
        }

        for (Theatre theatre : theatreList) {
            movieShowTimeResponseList.add(MovieShowTimeResponse.build(theatre, movieId));
        }
        return movieShowTimeResponseList;
    }

    @Transactional
    public SeatLayoutResponse getLayoutForShow(int showId) {
        ShowTime showTime = (ShowTime) movieDao.getEntityFromId(ShowTime.class, showId);
        SeatLayoutResponse response = SeatLayoutResponse.build(showTime);
        return response;
    }


    @Transactional
    public SeatLayoutResponse getLayoutShow(int showId) {
        ShowTime showTime = (ShowTime) movieDao.getEntityFromId(ShowTime.class, showId);
        SeatLayoutResponse response = SeatLayoutResponse.build(showTime);
        return response;
    }


    @Transactional
    public SeatDataResponse getLayoutDataShow(int showId) {
        ShowTime showTime = (ShowTime) movieDao.getEntityFromId(ShowTime.class, showId);
        SeatDataResponse response = SeatDataResponse.build(showTime);
        return response;
    }


    @Transactional
    public void createBooking(Booking booking) {
        movieDao.createBooking(booking);
    }

    public ShowTime getShowTime(Integer showID) {
        ShowTime showTimeFromId = (ShowTime) movieDao.getEntityFromId(ShowTime.class,
                showID);
        if (showTimeFromId == null) {
            throw new UserProfileException(ApiMessageCodeEnum.SHOW_NOT_AVAILABLE);
        }
        return showTimeFromId;
    }

    @Transactional
    public void confirmBooking(BookingRequest request, String email) {
        Member member = userProfileService.getMember(email);
        List<Seat> seats = seatService.validateSeatsAvailability(request.getSeats());
        Double amount = getTotalPrice(seats);
        Double fee = amount * FEE_PERCENT;
        Double totalAmnt = amount + fee;
        PayModeType payMode = getPayMode(request.getPayModeRequest().getPayModeId());
        paymentService.doPayment(request.getPayModeRequest(), totalAmnt);
        seatService.bookSeats(request.getSeats());
        ShowTime showTime = getShowTime(request.getShowId());

        BookingStatusEnum status = payMode == PayModeType.COT ? BookingStatusEnum.CREATED : BookingStatusEnum.BOOKED;
        Booking booking = Booking.create(amount, member, seats,
                totalAmnt, fee, showTime, status, payMode);
        createBooking(booking);
        showTime.getBookings().add(booking);
        member.getBookings().add(booking);
    }

    private PayModeType getPayMode(Integer payModeId) {
        return PayModeType.getPayMode(payModeId);
    }


    @Transactional
    public List<BookingDetail> allHistory(String email) {
        Member member = userProfileService.getMember(email);
        if (member == null) {
            throw new UserProfileException(ApiMessageCodeEnum.INVALID_LOGIN_ID);
        }
        List<BookingDetail> bookingDetailList = new ArrayList<>();
        List<Booking> bookings = member.getBookings();
        if (CollectionUtils.isEmpty(bookings)) {
            return bookingDetailList;
        }
        for (Booking booking : bookings) {
            bookingDetailList.add(BookingDetail.build(booking));
        }
        return bookingDetailList;
    }
}
