package com.cinematalkies.cinema.service;

import com.cinematalkies.cinema.dao.SeatDao;
import com.cinematalkies.cinema.model.entity.Seat;
import com.cinematalkies.profile.model.enums.ApiMessageCodeEnum;
import com.cinematalkies.profile.model.exception.UserProfileException;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * Created by maninder on 23/1/17.
 */
@Service
public class SeatService {

    @Autowired
    private SeatDao seatDao;

    @Transactional
    List<Seat> validateSeatsAvailability(Set<Integer> seats) {
        List<Seat> list = seatDao.getAvailableSeats(seats);
        if (CollectionUtils.isEmpty(list) || list.size() != seats.size()) {
            throw new UserProfileException(ApiMessageCodeEnum.SEAT_NOT_AVAILABLE);
        }
        return list;
    }


    @Transactional
    public void bookSeats(Set<Integer> seats) {
        int bookedSeats = seatDao.bookSeats(seats);
        if (bookedSeats != seats.size()) {
            throw new UserProfileException(ApiMessageCodeEnum.SEAT_NOT_AVAILABLE);
        }
    }
}
