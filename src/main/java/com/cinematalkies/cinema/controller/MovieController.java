package com.cinematalkies.cinema.controller;

import com.cinematalkies.cinema.model.PayModeType;
import com.cinematalkies.cinema.model.entity.Movie;
import com.cinematalkies.cinema.model.request.BookingRequest;
import com.cinematalkies.cinema.model.response.BookingDetail;
import com.cinematalkies.cinema.model.response.MovieShowTimeResponse;
import com.cinematalkies.cinema.service.MovieService;
import com.cinematalkies.profile.controller.ControllerBaseUserProfile;
import com.cinematalkies.profile.model.enums.ApiMessageCodeEnum;
import com.cinematalkies.profile.model.exception.UserProfileException;
import com.cinematalkies.profile.model.request.LoginRequest;
import com.cinematalkies.profile.model.response.RestApiResponse;
import com.cinematalkies.profile.service.LoginService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by maninder on 22/1/17.
 */
@Controller
@RequestMapping("movie")
public class MovieController extends ControllerBaseUserProfile {

    public static final String TEST_ID = "m@gmail.com";

    @Autowired
    private MovieService movieService;

    @Autowired
    private LoginService loginService;

    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    RestApiResponse getMovies(HttpServletRequest servletRequest) {
        preHandle(servletRequest);
        List<Movie> allMovies = movieService.getAllDatas(Movie.class);
        return RestApiResponse.buildSuccess(allMovies);
    }

    @RequestMapping(value = "shows/{id}", method = RequestMethod.GET)
    public
    @ResponseBody
    RestApiResponse getShowsForMovies(@PathVariable("id") int id, HttpServletRequest httpServletRequest) {
        preHandle(httpServletRequest);
        List<MovieShowTimeResponse> movieShowTimeResponseList = movieService.getShowsForMovieId(id);
        return RestApiResponse.buildSuccess(movieShowTimeResponseList);
    }


    @RequestMapping(value = "seat/{show_id}", method = RequestMethod.GET)
    public
    @ResponseBody
    RestApiResponse getLayoutForShow(@PathVariable("show_id") int showId, HttpServletRequest servletRequest) {
        preHandle(servletRequest);
        return RestApiResponse.buildSuccess(movieService.getLayoutForShow(showId));
    }


    @RequestMapping(value = "paymodes", method = RequestMethod.GET)
    public
    @ResponseBody
    RestApiResponse getPayModes() {
        return RestApiResponse.buildSuccess(PayModeType.values());
    }


    @RequestMapping(value = "book", method = RequestMethod.POST)
    public
    @ResponseBody
    RestApiResponse book(@RequestHeader("email") String email, @RequestBody BookingRequest request, HttpServletRequest servletRequest) {
        //String email = TEST_ID;
        preHandle(servletRequest);
        movieService.confirmBooking(request, email);
        return RestApiResponse.buildSuccess();
    }

    @RequestMapping(value = "history", method = RequestMethod.GET)
    public
    @ResponseBody
    RestApiResponse history(@RequestHeader("email") String email, HttpServletRequest servletRequest) {
        //String email = TEST_ID;
        preHandle(servletRequest);

        List<BookingDetail> bookingDetails = movieService.allHistory(email);

        return RestApiResponse.buildSuccess(bookingDetails);
    }


    public void preHandle(HttpServletRequest request) {
        String email = request.getHeader("email");
        String pwd = request.getHeader("pwd");
        if (StringUtils.isBlank(email) || StringUtils.isBlank(pwd))
            throw new UserProfileException(ApiMessageCodeEnum.UNAUTHORIZED_ACCESS);
        try {
            loginService.processLogin(LoginRequest.buildLoginRequest(email, pwd));
        } catch (Exception e) {
            throw new UserProfileException(ApiMessageCodeEnum.UNAUTHORIZED_ACCESS);
        }
    }
}
