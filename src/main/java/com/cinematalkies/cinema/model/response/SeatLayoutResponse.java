package com.cinematalkies.cinema.model.response;

import com.cinematalkies.cinema.model.entity.Seat;
import com.cinematalkies.cinema.model.entity.ShowTime;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by maninder on 23/1/17.
 */
public class SeatLayoutResponse {

    private LayoutRows layout;
    private Integer showId;
    private Integer theatreId;
    private Integer screenId;

    public Integer getShowId() {
        return showId;
    }

    public void setShowId(Integer showId) {
        this.showId = showId;
    }

    public Integer getTheatreId() {
        return theatreId;
    }

    public void setTheatreId(Integer theatreId) {
        this.theatreId = theatreId;
    }

    public Integer getScreenId() {
        return screenId;
    }

    public void setScreenId(Integer screenId) {
        this.screenId = screenId;
    }

    public LayoutRows getLayout() {
        return layout;
    }

    public void setLayout(LayoutRows layout) {
        this.layout = layout;
    }

    public static SeatLayoutResponse build(ShowTime showTime) {
        SeatLayoutResponse seatLayoutResponse = new SeatLayoutResponse();
        List<Seat> seats = showTime.getSeats();
        new Gson().toJson(seats);
        seatLayoutResponse.setShowId(showTime.getId());
        seatLayoutResponse.setLayout(LayoutRows.creat(seats));
        return seatLayoutResponse;
    }
}
