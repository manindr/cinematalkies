package com.cinematalkies.cinema.model.response;

import com.cinematalkies.cinema.model.entity.Seat;
import com.cinematalkies.cinema.model.entity.ShowTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maninder on 23/1/17.
 */
public class SeatDataResponse {

    private List<SeatData> seats;

    public List<SeatData> getSeats() {
        return seats;
    }

    public void setSeats(List<SeatData> seats) {
        this.seats = seats;
    }

    public static  List<SeatData> addeats(List<Seat> seatsOld) {
        List<SeatData> seats = new ArrayList<>();

        for (Seat s : seatsOld){
        seats.add(SeatData.c(s.getId()));
        }
        return seats;
    }


    public static SeatDataResponse build(ShowTime showTime) {
        SeatDataResponse seatLayoutResponse = new SeatDataResponse();
        seatLayoutResponse.setSeats(addeats(showTime.getSeats()));
        //seatLayoutResponse.setScreenId(showTime.getAvailableSeats());
        //Screen screen = showTime.getScreen();
        /*if(showTime.getScreen() != null ){
        *//*seatLayoutResponse.setScreenId(showTime.getScreen().getId());*//*
        }*/
        return seatLayoutResponse;
    }


    public static class  SeatData {
        private  Integer id;
        public  static  SeatData c(Integer id){
            SeatData s  = new SeatData() ;
            s.id = id;
            return s;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }
}
