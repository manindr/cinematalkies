package com.cinematalkies.cinema.model.response;

import com.cinematalkies.cinema.model.entity.Seat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maninder on 25/1/17.
 */
public class LayoutRows {
    private Integer columnNo;
    private Integer rowNo;
    private  List<Row> rows;

    public static LayoutRows creat(List<Seat> seats) {
        int colNo = -1 ;
        int rowNo = -1 ;
        for(Seat seat : seats ){
            if(seat.getxPosition() > colNo ) {
                colNo =seat.getxPosition();
            }

            if(seat.getyPosition() > rowNo ) {
                rowNo =seat.getxPosition();
            }
        }
        rowNo = rowNo + 1;
        colNo = colNo + 1;
        Seat[][] seat2D = new Seat[rowNo][colNo];
        List<Row> rows = new ArrayList<>(rowNo);

        for(Seat seat : seats ){
            if(seat2D[seat.getyPosition()] == null ){
                seat2D[seat.getyPosition()] = new Seat[colNo];
            }
            seat2D[seat.getyPosition()][seat.getxPosition()] = seat;
        }
        int j = 0;
        for(j =0 ;j < rowNo ;j++ ) {
            Row row = new Row();
            for( int i = 0 ;i<colNo ; i++){
                row.add(seat2D[j][i]);
            }
            rows.add(row);
            j++;
        }
        LayoutRows layoutRows = new LayoutRows();
        layoutRows.setColumnNo(colNo);
        layoutRows.setRowNo(rowNo);
        layoutRows.setRows(rows);
        return layoutRows;
    }

    public static  class Row{
        public  Row(){
            seats = new ArrayList<>();
        }
        private List<Seat> seats;
        public List<Seat> getSeats() {
            return seats;
        }

        public void add(Seat seat) {
            this.seats.add(seat);
        }

        public void setSeats(List<Seat> seats) {
            this.seats = seats;
        }
    }

    public Integer getColumnNo() {
        return columnNo;
    }

    public void setColumnNo(Integer columnNo) {
        this.columnNo = columnNo;
    }

    public Integer getRowNo() {
        return rowNo;
    }

    public void setRowNo(Integer rowNo) {
        this.rowNo = rowNo;
    }

    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }
}
