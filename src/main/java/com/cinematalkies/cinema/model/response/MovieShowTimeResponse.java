package com.cinematalkies.cinema.model.response;

import com.cinematalkies.cinema.model.entity.ShowTime;
import com.cinematalkies.cinema.model.entity.Theatre;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maninder on 23/1/17.
 */
public class MovieShowTimeResponse {

    private int theatreId;
    private String theatreName;
    private String theatreAddress;
    private List<ShowTime> showTimes;

    public int getTheatreId() {
        return theatreId;
    }

    public void setTheatreId(int theatreId) {
        this.theatreId = theatreId;
    }

    public String getTheatreName() {
        return theatreName;
    }

    public void setTheatreName(String theatreName) {
        this.theatreName = theatreName;
    }

    public String getTheatreAddress() {
        return theatreAddress;
    }

    public void setTheatreAddress(String theatreAddress) {
        this.theatreAddress = theatreAddress;
    }

    public List<ShowTime> getShowTimes() {
        return showTimes;
    }

    public void setShowTimes(List<ShowTime> showTimes) {
        this.showTimes = showTimes;
    }

    public static MovieShowTimeResponse build(Theatre theatre, int movieId) {
        MovieShowTimeResponse response = new MovieShowTimeResponse();
        response.setTheatreId(theatre.getId());
        response.setTheatreName(theatre.getName());
        response.setTheatreAddress(theatre.getAddress().getAddress());
        response.setShowTimes(filterShowTimes(theatre.getShowTimes(), movieId));
        return response;
    }

    private static List<ShowTime> filterShowTimes(List<ShowTime> showTimes, int movieId) {
        List<ShowTime> filteredShowTimes = new ArrayList<>();
        if (CollectionUtils.isEmpty(showTimes))
            return filteredShowTimes;
        for (ShowTime showTime : showTimes) {
            if (showTime.getMovie().getId().equals(movieId)) {
                filteredShowTimes.add(showTime);
            }
        }
        return filteredShowTimes;
    }
}
