package com.cinematalkies.cinema.model.response;

import com.cinematalkies.cinema.model.entity.Booking;
import com.cinematalkies.cinema.model.entity.Movie;
import com.cinematalkies.cinema.model.entity.ShowTime;
import com.cinematalkies.cinema.model.entity.Theatre;

import java.util.Date;

/**
 * Created by maninder on 26/1/17.
 */
public class BookingDetail {
    Integer bookingId;
    private ShowTime showTime;
    private Movie movie;
    private Theatre theatre;
    Date bookingAt;

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public ShowTime getShowTime() {
        return showTime;
    }

    public void setShowTime(ShowTime showTime) {
        this.showTime = showTime;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Theatre getTheatre() {
        return theatre;
    }

    public void setTheatre(Theatre theatre) {
        this.theatre = theatre;
    }

    public Date getBookingAt() {
        return bookingAt;
    }

    public void setBookingAt(Date bookingAt) {
        this.bookingAt = bookingAt;
    }

    public static BookingDetail build(Booking booking) {
        BookingDetail detail = new BookingDetail();
        ShowTime showTime = booking.getShowTime();
        detail.setShowTime(showTime);
        detail.setMovie(showTime.getMovie());
        detail.setBookingId(booking.getId());
        detail.setTheatre(showTime.getScreen().getTheatre());
        detail.setBookingAt(booking.getBookedAt());
        return detail;
    }
}
