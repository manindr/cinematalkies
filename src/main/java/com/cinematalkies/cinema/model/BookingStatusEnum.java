package com.cinematalkies.cinema.model;

/**
 * Created by maninder on 23/1/17.
 */
public enum  BookingStatusEnum {
    CREATED(1),
    BOOKED(2),
    CANCELLED(3);

    private Integer id;

    BookingStatusEnum(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
