package com.cinematalkies.cinema.model.entity;

import com.cinematalkies.cinema.model.BookingStatusEnum;
import com.cinematalkies.cinema.model.PayModeType;
import com.cinematalkies.profile.model.entity.Member;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by maninder on 22/1/17.
 */
@Entity
@Table(name = "booking")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    @JsonIgnore
    private Member member;

    private int payMode;

    private Double totalAmount;

    private Double amount;

    private Double fee;

    @OneToMany
    @JsonIgnore
    private List<Seat> seats;

    @ManyToOne
    @JsonIgnore
    private ShowTime showTime;

    private int status;

    private Date bookedAt;


    public static Booking create(Double amount, Member member,
                                 List<Seat> seats, Double totalAmount,
                                 Double fee, ShowTime showTime, BookingStatusEnum status, PayModeType payMode) {
        Booking bookingEntity = new Booking();
        bookingEntity.bookedAt = new Date();
        bookingEntity.totalAmount = totalAmount;
        bookingEntity.seats = seats;
        bookingEntity.payMode = payMode.getId();
        bookingEntity.member = member;
        bookingEntity.amount = amount;
        bookingEntity.fee = fee;
        bookingEntity.status = status.getId();
        bookingEntity.showTime = showTime;
        return bookingEntity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public int getPayMode() {
        return payMode;
    }

    public void setPayMode(int payMode) {
        this.payMode = payMode;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public List<Seat> getSeats() {
        return seats;
    }

    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }

    public ShowTime getShowTime() {
        return showTime;
    }

    public void setShowTime(ShowTime showTime) {
        this.showTime = showTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getBookedAt() {
        return bookedAt;
    }

    public void setBookedAt(Date bookedAt) {
        this.bookedAt = bookedAt;
    }
}
