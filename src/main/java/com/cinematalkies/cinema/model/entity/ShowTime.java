
package com.cinematalkies.cinema.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by maninder on 22/1/17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table(name = "showTime")
public class ShowTime {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Date date;

    @JsonIgnore
    @OneToOne
    private Screen screen;

    @JsonIgnore
    @OneToMany
    private List<Seat> seats;

    @JsonIgnore
    @OneToOne
    private Movie movie;

    @JsonIgnore
    @OneToMany
    private List<Booking> bookings;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Screen getScreen() {
        return screen;
    }

    public void setScreen(Screen screen) {
        this.screen = screen;
    }

    public List<Seat> getSeats() {
        return seats;
    }

    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }

    public static ShowTime build(Screen screen, Timestamp timestamp,Movie movie) {
        ShowTime showTime = new ShowTime();
        showTime.setDate(new Date(timestamp.getTime()));
        showTime.setScreen(screen);
        showTime.setMovie(movie);
        return showTime;
    }
}