package com.cinematalkies.cinema.model.entity;

/**
 * Created by maninder on 22/1/17.
 */

public enum SeatType {

    GOLD(1), SILVER(2), PLATINUM(3);

    private int type;

    SeatType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
