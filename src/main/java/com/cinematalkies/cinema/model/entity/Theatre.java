package com.cinematalkies.cinema.model.entity;

import com.cinematalkies.profile.model.request.TheatreRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.List;

/**
 * Created by maninder on 22/1/17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table(name = "theatre")
public class Theatre {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @OneToMany
    @JsonIgnore
    private List<Screen> screens;

    @ManyToMany
    @JsonIgnore
    private List<Movie> movies;

    @OneToOne
    @JsonIgnore
    private Address address;

    @OneToMany
    @JsonIgnore
    private List<ShowTime> showTimes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Screen> getScreens() {
        return screens;
    }

    public void setScreens(List<Screen> screens) {
        this.screens = screens;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public List<ShowTime> getShowTimes() {
        return showTimes;
    }

    public void setShowTimes(List<ShowTime> showTimes) {
        this.showTimes = showTimes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Theatre build(TheatreRequest theatreRequest) {
        Theatre theatre = new Theatre();
        theatre.setName(theatreRequest.getName());
        theatre.setAddress(theatreRequest.getAddress());
        return theatre;
    }

}
