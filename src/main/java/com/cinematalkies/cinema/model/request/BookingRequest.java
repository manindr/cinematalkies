package com.cinematalkies.cinema.model.request;

import java.util.Set;

/**
 * Created by maninder on 23/1/17.
 */

public class BookingRequest {

    private Integer showId;
    private Set<Integer> seats;
    private PayModeRequest payModeRequest;

    public PayModeRequest getPayModeRequest() {
        return payModeRequest;
    }

    public void setPayModeRequest(PayModeRequest payModeRequest) {
        this.payModeRequest = payModeRequest;
    }

    public Set<Integer> getSeats() {
        return seats;
    }

    public void setSeats(Set<Integer> seats) {
        this.seats = seats;
    }

    public Integer getShowId() {
        return showId;
    }

    public void setShowId(Integer showId) {
        this.showId = showId;
    }
}
