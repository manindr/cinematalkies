package com.cinematalkies.cinema.model.request;

import java.util.Map;

/**
 * Created by maninder on 23/1/17.
 */
public class PayModeRequest {
    private Integer payModeId;
    private Map<String, Object> bankingDetails;

    public Integer getPayModeId() {
        return payModeId;
    }

    public void setPayModeId(Integer payModeId) {
        this.payModeId = payModeId;
    }

    public Map<String, Object> getBankingDetails() {
        return bankingDetails;
    }

    public void setBankingDetails(Map<String, Object> bankingDetails) {
        this.bankingDetails = bankingDetails;
    }
}
