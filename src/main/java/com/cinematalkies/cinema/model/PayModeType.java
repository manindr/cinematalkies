package com.cinematalkies.cinema.model;

/**
 * Created by maninder on 23/1/17.
 */
public enum PayModeType {
    COT(1),
    ONLINE_PAYMENT(2);

    private int id;

    PayModeType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static PayModeType getPayMode(Integer payModeId) {
        switch (payModeId) {
            case 1:
                return COT;
            case 2:
                return ONLINE_PAYMENT;
        }
        return ONLINE_PAYMENT;
    }
}
