package com.cinematalkies.cinema.dao;

import com.cinematalkies.cinema.model.entity.Booking;
import com.cinematalkies.cinema.model.entity.Movie;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by maninder on 22/1/17.
 */
@Repository
public class MovieDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    public void updateEntity(Object o) {
        getCurrentSession().update(o);
    }

    public List getAllDatas(Class clazz) {
        return getCurrentSession().createCriteria(clazz).list();
    }

    public Movie getMovie(int movieId) {
        String hql = "from Movie where id=:id";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("id", movieId);
        return (Movie) query.uniqueResult();
    }

    public Object getEntityFromId(Class T, int showId) {
        return getCurrentSession().get(T, showId);
    }

    public void createBooking(Booking booking) {
        getCurrentSession().save(booking);
    }

    public void save(Object object){
        getCurrentSession().save(object);
    }
}
