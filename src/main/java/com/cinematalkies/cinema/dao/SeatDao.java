package com.cinematalkies.cinema.dao;

import com.cinematalkies.cinema.model.entity.Seat;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Created by maninder on 23/1/17.
 */
@Repository
public class SeatDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    public List<Seat> getAvailableSeats(Set<Integer> seats) {
        String hql = "from Seat where isAvailable = true  and id in(:seats)";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameterList("seats", seats);
        List list = query.list();
        return list;
    }

    public int bookSeats(Set<Integer> seats) {
        String hql = "update Seat set isAvailable=false where isAvailable = true  and id in(:seats)";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameterList("seats", seats);
        return query.executeUpdate();
    }

}
