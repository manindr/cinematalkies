package com.cinematalkies.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by maninder on 22/1/17.
 */
public class EmailUtils {

    // To validate the email id whether it is in correct format
    public static boolean validateEmailFormat(String emailId) {
        Pattern p = Pattern.compile("^[A-Za-z0-9]+((\\.|-|_)?[A-Za-z0-9]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher m = p.matcher(emailId);
        boolean matchFound = m.matches();

        if (matchFound) {
            return true;
        } else {
            return false;
        }
    }

}
