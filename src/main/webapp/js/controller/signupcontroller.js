Cinema_Talkies.controller('signUpController', function ($scope, $state, signupService) {
    $scope.signUpData = {};
    $scope.signUpButton = function () {
        $scope.userDetails = {
            email: $scope.signUpData.email,
            pwd: $scope.signUpData.password,
            address: $scope.signUpData.address,
            cell: $scope.signUpData.mobile_number
        };
        if ($scope.signUpData.email != undefined && $scope.signUpData.password != undefined && $scope.signUpData.address != undefined && $scope.signUpData.mobile_number != undefined) {

            signupService.signUp($scope.userDetails).then(function success(data) {
                console.log(data);
                if (data.success == true) {
//                    $("#success").modal("show");
                    localStorage.setItem('email',$scope.userDetails.email);
                    localStorage.setItem('pwd',$scope.userDetails.pwd);
                    $state.go('movies');
                }
                else {
                    var reason = "Sign up failed. Reason : " + data.message.text;
                    $("#reason").text(reason);
                    $("#failure").modal("show");
                }
            }, function error(error) {
                var reason = "Something went bad";
                $("#reason").text(reason);
                $("#failure").modal("show");
            });
        }

        console.log("====userequest====", $scope.userDetails);
    }
});