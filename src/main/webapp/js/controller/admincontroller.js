Cinema_Talkies.controller('adminController', function ($scope, $state, adminService) {
    $scope.movie = {
        name: '',
        duration: '',
        imagePath: ''
    };

    $scope.movies = [];

    $scope.showTimes = [];
    $scope.theatreShowTimes = [];
    $scope.theatres = [];
    $scope.theatre = {
        name: '',
        address: {
            city: '',
            state: ''
        }
    };

    adminService.getAllMovies().then(function success(data) {
        if (data.success == true) {
            $scope.movies = data.data;
        }
    });

    adminService.getAllTheatres().then(function success(data) {
        if (data.success == true) {
            $scope.theatres = data.data;
        }
    });

    $scope.addMovie = function () {
        console.log($scope.movie);
        adminService.addMovie($scope.movie).then(function success(data) {
            if (data.success == true) {
                $("#success").modal("show");
            }
            else {
                var reason = "Operation failed. Reason : " + data.message.text;
                $("#reason").text(reason);
                $("#failure").modal("show");
            }
        }, function failure() {
            var reason = "Something went bad";
            $("#reason").text(reason);
            $("#failure").modal("show");
        });
    };

    $scope.addTheatre = function () {
        console.log($scope.theatre);
        adminService.addTheatre($scope.theatre).then(function success(data) {
            if (data.success == true) {
                $("#success").modal("show");
            }
            else {
                var reason = "Operation failed. Reason : " + data.message.text;
                $("#reason").text(reason);
                $("#failure").modal("show");
            }
        }, function failure() {
            var reason = "Something went bad";
            $("#reason").text(reason);
            $("#failure").modal("show");
        });
    };

    $scope.addMovieToTheatre = function (movie, theatre) {
        console.log(movie + " " + theatre);
        var request = {};
        request.movieId = movie.id;
        request.theatreId = theatre.id;
        request.showTimes = $scope.showTimes;
        adminService.addMovieToTheatre(request).then(function success(data) {
            if (data.success == true) {
                $("#success").modal("show");
            }
            else {
                var reason = "Operation failed. Reason : " + data.message.text;
                $("#reason").text(reason);
                $("#failure").modal("show");
            }
        }, function failure() {
            var reason = "Something went bad";
            $("#reason").text(reason);
            $("#failure").modal("show");
        });
    };

    $scope.addShowTime = function (time) {
        console.log(time);
        console.log($scope.showTimes);
        var time2 = new Date(time).getTime();
        console.log(time2);
        $scope.showTimes.push(time2);
        console.log($scope.showTimes);
    };


    $scope.fetchShowsForTheatre = function (theatre) {
        adminService.fetchShowsForTheatre(theatre.id).then(function success(data) {
            if (data.success == true) {
                $scope.theatreShowTimes = data.data;
            }
        });

    };

    $scope.cancelBookings=function (show) {
        console.log(show);
        adminService.cancelBookings(show.id).then(function success(data) {
            if (data.success == true) {
                $("#success").modal("show");
            }
            else {
                var reason = "Operation failed. Reason : " + data.message.text;
                $("#reason").text(reason);
                $("#failure").modal("show");
            }
        }, function failure() {
            var reason = "Something went bad";
            $("#reason").text(reason);
            $("#failure").modal("show");
        });
    };

    $scope.payForBooking=function (bookingId) {
        console.log(bookingId);
        adminService.payForBooking(bookingId).then(function success(data) {
            if (data.success == true) {
                $("#success").modal("show");
            }
            else {
                var reason = "Operation failed. Reason : " + data.message.text;
                $("#reason").text(reason);
                $("#failure").modal("show");
            }
        }, function failure() {
            var reason = "Something went bad";
            $("#reason").text(reason);
            $("#failure").modal("show");
        });
    };

});