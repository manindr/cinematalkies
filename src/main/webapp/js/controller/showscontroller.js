Cinema_Talkies.controller('showsController', function ($scope, $stateParams, $filter,$rootScope, showService,$location) {
    var email = localStorage.getItem('email');
    var pwd = localStorage.getItem('pwd');
    var userLoginDetail={};
    userLoginDetail.email=email;
    userLoginDetail.pwd=pwd;
    var getMovieIndex = $stateParams.id;
    console.log("==getMovieIndex==", getMovieIndex);

//    $scope.newDate = $filter('date')(1485120959000, "dd/MM/yyyy HH:mm");
    console.log("==date-=", $scope.newDate);
//    console.log("===$scope.fetchshows==", $scope.fetchshows.data);
    showService.fetchShows(userLoginDetail, getMovieIndex).then(function success(data) {
        if (data.success == true) {
            $scope.fetchingDetails = data.data;
            console.log("===movie==name==",$scope.fetchingDetails.theatreName);
        }
        else {
            var reason = data.message.text;
            $("#freason").text(reason);
            $("#accessFailure").modal("show");
        }
    }, function failure(error) {
        var reason = "Something went bad";
        $("#freason").text(reason);
        $("#accessFailure").modal("show");
    });

    $scope.fetchSeats=function (show,theatreName) {
        console.log("time",show.date,theatreName);
        $rootScope.getShowTimings = show.date;
        $rootScope.getTheatreName = theatreName;

        $location.path('/seats/' + show.id);
    }

    $scope.back = function(){
        window.history.back();
    }
    $scope.getCount = function (n) {
        return new Array(n);
    }

});