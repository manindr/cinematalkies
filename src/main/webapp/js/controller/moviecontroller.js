Cinema_Talkies.controller('movieController', function ($scope, movieService, $location,$rootScope) {
    var email = localStorage.getItem('email');
    var pwd = localStorage.getItem('pwd');
    var userLoginDetail={};
    userLoginDetail.email=email;
    userLoginDetail.pwd=pwd;
    console.log("===userLoginDetail", userLoginDetail);
    $scope.result = {};
//    console.log("======$scope.result========", $scope.result.data[0].name);

    movieService.getMovie(userLoginDetail).then(function success(data) {
        console.log("==============", data);
        if (data.success == true) {
            $scope.movieDetails = data.data;
        }
        else {
            var reason = data.message.text;
            $("#freason").text(reason);
            $("#accessFailure").modal("show");
        }
    }, function failure(error) {
        var reason = "Something went bad";
        $("#freason").text(reason);
        $("#accessFailure").modal("show");
    });

    $scope.fetchMovieDetails = function (id,movieName,movieRating) {
        console.log("===fetch====", id,movieName,movieRating);
        $rootScope.getMovieName = movieName;
        $rootScope.getMovieRating = movieRating;
        $location.path('/shows/' + id);
    };
    $scope.isActive = function (route) {
        return route === $location.path();
    }
});