Cinema_Talkies.controller('bookingHistoryController', function($scope,$location,bookingHistoryService){
    $scope.isActive = function (route) {
        return route === $location.path();
    }
    var email = localStorage.getItem('email');
    var pwd = localStorage.getItem('pwd');
    var userLoginDetail={};
    userLoginDetail.email=email;
    userLoginDetail.pwd=pwd;
    console.log(userLoginDetail);
    bookingHistoryService.getHistory(userLoginDetail).then(function success(data){
        if(data.success == true){
            $scope.bookingHistory = data.data;
        }
        else {
            var reason = data.message.text;
            $("#freason").text(reason);
            $("#accessFailure").modal("show");
        }
    },function failure(error){
        var reason = "Something went bad";
        $("#freason").text(reason);
        $("#accessFailure").modal("show");
    });
});