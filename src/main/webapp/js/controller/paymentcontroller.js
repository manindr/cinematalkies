Cinema_Talkies.controller('paymentController', function ($scope, $stateParams, $filter, $state, paymentService) {
    var email = localStorage.getItem('email');
    var pwd = localStorage.getItem('pwd');
    var userLoginDetail = {};
    userLoginDetail.email = email;
    userLoginDetail.pwd = pwd;

    var data = $stateParams.data;
    console.log(data);
    var bookingRequest = {};
    bookingRequest.seats = data.seats;
    bookingRequest.showId = data.showId;
    $scope.payViaOnlineBanking = function (card) {
        var payModeRequest = {};
        payModeRequest.payModeId = 2;
        payModeRequest.bankingDetails = {card: card};
        bookingRequest.payModeRequest = payModeRequest;
        console.log(bookingRequest);
        paymentService.bookSeat(userLoginDetail,bookingRequest).then(function success(data) {
            if (data.success == true) {
                $("#success").modal("show");
            }
            else {
                $("#failure").modal("show");
            }
        }, function failure() {
            $("#failure").modal("show");
        });
    };

    $scope.payViaCOT = function () {
        var payModeRequest = {};
        payModeRequest.payModeId = 1;
        bookingRequest.payModeRequest = payModeRequest;
        console.log("cot mode selected");
        console.log(bookingRequest);
        paymentService.bookSeat(userLoginDetail,bookingRequest).then(function success(data) {
            if (data.success == true) {
                $("#success").modal("show");
            }
            else {
                $("#failure").modal("show");
            }
        }, function failure() {
            $("#failure").modal("show");
        });
    };

});