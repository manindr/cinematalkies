Cinema_Talkies.controller('seatController', function ($scope, $state, $stateParams,$rootScope, seatService, $location) {
    var email = localStorage.getItem('email');
    var pwd = localStorage.getItem('pwd');
    var userLoginDetail = {};
    userLoginDetail.email = email;
    userLoginDetail.pwd = pwd;
    console.log("===userLoginDetail", userLoginDetail);
    $scope.result = {};
    $scope.seats = {};
    $scope.customStyle = [];

    $scope.selecetedSeats = [];
    var showId = $stateParams.id;

    seatService.getSeats(userLoginDetail, showId).then(function success(data) {
        if (data.success == true) {
            $scope.seats = data.data;
        }
        else {
            console.log(data.data);
        }
    }, function failure(error) {
        console.log(error);
    });

    $scope.proceed = function () {
        $state.go('payment', {data: {showId: showId, seats: $scope.selecetedSeats}});
    };

    $scope.selectSeat = function (seat) {
        console.log(seat);
        $scope.selecetedSeats.push(seat.id);
        console.log($scope.selecetedSeats);
        $rootScope.seatsSeletion = $scope.selecetedSeats.length;
        console.log("===",$rootScope.seatsSeletion);
    };

    $scope.notBooked = function (seat) {
        for (var i = 0; i < $scope.selecetedSeats.length; i++) {
            if ($scope.selecetedSeats[i] == seat.id) {
                return false;
            }
        }
        return true;
    };

    $scope.fetchSeatDetails = function (id) {
        console.log("===fetch====", id);
        $location.path('/seats/' + id);
    };

});