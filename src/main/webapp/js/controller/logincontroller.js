Cinema_Talkies.controller('loginController', function ($scope,$state, loginService) {
    $scope.loginData = {};
    $scope.loginButton = function () {
        $scope.loginDetails = {
            email: $scope.loginData.email,
            pwd: $scope.loginData.password
        };
        if ($scope.loginData.email != undefined && $scope.loginData.password != undefined) {
            loginService.Login($scope.loginDetails).then(function success(data) {
                if (data.success == true) {
//                    $("#success").modal("show");
                    localStorage.setItem('email',$scope.loginDetails.email);
                    localStorage.setItem('pwd',$scope.loginDetails.pwd);
                    $state.go('movies');
                }
                else {
                    var reason = "Login failed. Reason : " + data.message.text;
                    $("#reason").text(reason);
                    $("#failure").modal("show");
                }
            }, function failure() {
                var reason = "Something went bad";
                $("#reason").text(reason);
                $("#failure").modal("show");
            });
        }

        console.log("====", $scope.loginDetails);
    }
});