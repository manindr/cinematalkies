Cinema_Talkies.factory('bookingHistoryService', function($q,$http){
   var self = this;

   var defered = $q.defer();
   self.getHistory = function(userdetail) {

       $http({
           method:'GET',
           url:'/p/movie/history',
           headers:userdetail
       })
           .then(function success(response){
               defered.resolve(response.data);
           },function failure(reason){
               defered.reject(reason);
           }).finally(function(){

       });
       return defered.promise;
   }
   return self;
});