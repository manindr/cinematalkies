Cinema_Talkies.factory('movieService', function ($q, $http) {

    var self = this;
    var testData = {
        "data": [
            {
                "id": 1,
                "name": "Dangal",
                "rating": 4,
                "img": "fasfdd",
                "type": "Thriller|Action",
                "duration": 140
            },
            {
                "id": 2,
                "name": "Raees",
                "rating": 3,
                "img": "fasf",
                "type": "Thriller|Action",
                "duration": 140
            },
            {
                "id": 3,
                "name": "Dear Zindgi",
                "rating": 4,
                "img": "fvjkfdk",
                "type": "Thriller|Action",
                "duration": 140
            },
            {
                "id": 4,
                "name": "xxx",
                "rating": 4,
                "img": "fvjkfdk",
                "type": "Thriller|Action",
                "duration": 140
            }
        ]

    };
    self.getMovie = function (loginData) {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: "/p/movie",
            headers:loginData
        }).then(function success(response) {
            deferred.resolve(response.data);
        }, function failure(reason) {
            deferred.reject(reason);
        }).finally(function () {

        });
        return deferred.promise;
    };
    return self;
});