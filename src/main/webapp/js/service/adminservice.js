Cinema_Talkies.factory('adminService', function ($q, $http) {

    var self = this;

    function getUrl(url) {
        return "/p/admin/" + url;
    }

    self.addMovie = function (request) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: getUrl("movie"),
            data: request
        }).then(function success(response) {
            deferred.resolve(response.data);
        }, function failure(reason) {
            deferred.reject(reason);
        }).finally(function () {

        });
        return deferred.promise;
    };

    self.addTheatre = function (request) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: getUrl("theatre"),
            data: request
        }).then(function success(response) {
            deferred.resolve(response.data);
        }, function failure(reason) {
            deferred.reject(reason);
        }).finally(function () {

        });
        return deferred.promise;
    };

    self.getAllMovies = function () {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: '/p/movie'
        }).then(function success(response) {
            deferred.resolve(response.data);
        }, function failure(reason) {
            deferred.reject(reason);
        }).finally(function () {

        });
        return deferred.promise;
    };

    self.getAllTheatres = function () {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: getUrl("theatre")
        }).then(function success(response) {
            deferred.resolve(response.data);
        }, function failure(reason) {
            deferred.reject(reason);
        }).finally(function () {

        });
        return deferred.promise;
    };

    self.addMovieToTheatre = function (request) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: getUrl("movieToTheatre"),
            data: request
        }).then(function success(response) {
            deferred.resolve(response.data);
        }, function failure(reason) {
            deferred.reject(reason);
        }).finally(function () {

        });
        return deferred.promise;
    };

    self.fetchShowsForTheatre = function (id) {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: getUrl('theatre/shows/'+id)
        }).then(function success(response) {
            deferred.resolve(response.data);
        }, function failure(reason) {
            deferred.reject(reason);
        }).finally(function () {

        });
        return deferred.promise;
    };

    self.cancelBookings=function (id) {
        var deferred = $q.defer();
        $http({
            method: 'DELETE',
            url: getUrl('cancelUnpaidBookings/'+id)
        }).then(function success(response) {
            deferred.resolve(response.data);
        }, function failure(reason) {
            deferred.reject(reason);
        }).finally(function () {

        });
        return deferred.promise;
    };

    self.payForBooking=function (id) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: getUrl('pay/'+id)
        }).then(function success(response) {
            deferred.resolve(response.data);
        }, function failure(reason) {
            deferred.reject(reason);
        }).finally(function () {

        });
        return deferred.promise;
    };
    return self

});