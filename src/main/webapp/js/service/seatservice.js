//noinspection JSUnresolvedFunction
Cinema_Talkies.factory('seatService', function ($q, $http) {
    var self = this;

    self.getSeats = function (userDetail, id) {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: "/p/movie/seat/" + id,
            headers: userDetail
        }).then(function success(response) {
            deferred.resolve(response.data);
        }, function failure(reason) {
            deferred.reject(reason);
        }).finally(function () {

        });
        return deferred.promise;

    };
    return self;
});

