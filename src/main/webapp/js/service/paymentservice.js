//noinspection JSUnresolvedFunction
Cinema_Talkies.factory('paymentService', function ($q, $http) {
    var self = this;

    self.bookSeat = function (userDetail,request) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: "/p/movie/book",
            data:request,
            headers:userDetail
        }).then(function success(response) {
            deferred.resolve(response.data);
        }, function failure(reason) {
            deferred.reject(reason);
        }).finally(function () {

        });
        return deferred.promise;

    };
    return self;
});

