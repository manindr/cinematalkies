Cinema_Talkies.factory('signupService', function($http,$q){

	var self = this;

	function getUrl() {
		return "/p/account/sign-up";
	}
	self.signUp = function(request){
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: getUrl(""),
            data: request
        }).then(function success(response) {
            deferred.resolve(response.data);
        }, function failure(reason) {
            deferred.reject(reason);
        }).finally(function () {

        });
        return deferred.promise;

	};
	return self;
});