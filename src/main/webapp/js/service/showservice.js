//noinspection JSUnresolvedFunction
Cinema_Talkies.factory('showService', function ($q, $http) {
    var self = this;

    self.fetchShows = function (userDetail, id) {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: "/p/movie/shows/" + id,
            headers:userDetail
        }).then(function success(response) {
            deferred.resolve(response.data);
        }, function failure(reason) {
            deferred.reject(reason);
        }).finally(function () {

        });
        return deferred.promise;

    };
    return self;
});

/*
 $scope.fetchshows = {
 "success": true,
 "data": [
 {
 "theatreId": 1,
 "theatreName": "Odean",
 "theatreAddress": "Delhi,Delhi",
 "showTimes": [
 {
 "id": 1,
 "date": 1485113759000
 },
 {
 "id": 5,
 "date": 1485120959000
 }
 ]
 },
 {
 "theatreId": 2,
 "theatreName": "PVR MGF",
 "theatreAddress": "gurgaon,Haryana",
 "showTimes": [
 {
 "id": 3,
 "date": 1485113759000
 },
 {
 "id": 7,
 "date": 1485120959000
 }
 ]
 },
 {
 "theatreId": 3,
 "theatreName": "Savitri",
 "theatreAddress": "GK1",
 "showTimes": [
 {
 "id": 9,
 "date": 1485113759000
 },
 {
 "id": 11,
 "date": 1485120959000
 }
 ]
 },
 {
 "theatreId": 4,
 "theatreName": "PVR MGF",
 "theatreAddress": "gurgaon,Haryana",
 "showTimes": [
 {
 "id": 13,
 "date": 1485113759000
 },
 {
 "id": 15,
 "date": 1485120959000
 }
 ]
 }
 ]
 }
 */
