var Cinema_Talkies = angular.module('cinema_talkies', ['ui.router', 'angularjs-datetime-picker']);

Cinema_Talkies.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('signup', {
            url: "/signup",
            templateUrl: 'signup.html',
            controller: 'signUpController'
        })

        .state('login', {
            url: "/login",
            templateUrl: 'login.html',
            controller: 'loginController'
        })

        .state('movies', {
            url: "/movies",
            templateUrl: 'movies.html',
            controller: 'movieController'
        })

        .state('bookinghistory', {
            url: '/bookinghistory',
            templateUrl: 'bookinghistory.html',
            controller: 'bookingHistoryController'
        })

        .state('shows', {
            url: '/shows/:id',
            templateUrl: 'shows.html',
            controller: 'showsController'
        })
        .state('admin', {
            url: '/admin',
            templateUrl: 'admin.html',
            controller: 'adminController'
        })
        .state('seats', {
                url: '/seats/:id',
                templateUrl: 'seat.html',
                controller: 'seatController'
            }
        ).state('payment', {
            url: '/payment',
            templateUrl: 'payment.html',
            controller: 'paymentController',
            params: {
                data: null
            }
        }
    );

    $urlRouterProvider.otherwise('/signup');
}]);
