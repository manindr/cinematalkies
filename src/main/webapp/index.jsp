<html>
<head>
    <title>CINEMA TALKIES</title>
    <link rel="stylesheet" href="css/style.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/angularjs-datetime-picker.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.15/angular-ui-router.min.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    <script src="${pageContext.request.contextPath}/js/angularjs-datetime-picker.js"></script>

    <!-- controllers -->
    <script type="text/javascript" src="js/controller/logincontroller.js"></script>
    <script type="text/javascript" src="js/controller/signupcontroller.js"></script>
    <script type="text/javascript" src="js/controller/moviecontroller.js"></script>
    <script type="text/javascript" src="js/controller/showscontroller.js"></script>
    <script type="text/javascript" src="js/controller/admincontroller.js"></script>
    <script type="text/javascript" src="js/controller/seatcontroller.js"></script>
    <script type="text/javascript" src="js/controller/paymentcontroller.js"></script>
    <script type="text/javascript" src="js/controller/bookinghistorycontroller.js"></script>

    <!-- services -->
    <script type="text/javascript" src="js/service/loginservice.js"></script>
    <script type="text/javascript" src="js/service/signupservice.js"></script>
    <script type="text/javascript" src="js/service/movieservice.js"></script>
    <script type="text/javascript" src="js/service/showservice.js"></script>
    <script type="text/javascript" src="js/service/adminservice.js"></script>
    <script type="text/javascript" src="js/service/seatservice.js"></script>
    <script type="text/javascript" src="js/service/paymentservice.js"></script>
    <script type="text/javascript" src="js/service/bookinghistoryservice.js"></script>
</head>
<body ng-app="cinema_talkies">
<div data-ui-view=""></div>

<div id="accessFailure" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="span12 text-center"><a href="#">
                    <span class="glyphicon  glyphicon-alert"></span>
                </a>
                </div>
                <div class="span12 text-center">
                    <p style="color:#c9302c">Alert</p>
                </div>
                <div class="span12 text-center">
                    <p id="freason">
                    </p>
                </div>
                <button type="button" class="btn btn-info btn-block" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
